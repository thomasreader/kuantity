# kuantity

A unit of measurement and [quantity](https://en.wikipedia.org/wiki/List_of_physical_quantities) library to convert and manipulate 64-bit floating-point (double) quantities using [Kotlin inline classes](https://kotlinlang.org/docs/inline-classes.html). 

Usually quantities are represented by a value and a unit (50 cm) but by using just an inline value they are effectively represented by the value multiplied by the reference unit (most commonly a SI unit): so 50 cm will be represented as 0.5 m should metre be defined as the reference.

The current set of unit classes are relatively basic consisting of a symbol and a conversion — chosen because the symbol is locale agnostic. This could be extended by adding in names or Android string resource id's as well as implementing a derived unit composed of units and powers.

Additional functions can be added to more easily to multiply or divide quantities to create quantities of a different dimension e.g. :
```kotlin
public inline operator fun Quantity<Length>.times(that: Quantity<Length>): Quantity<Area> {
    return Quantity<T>(this.value * that.value)
}
// usage
val area/*: Quantity<Area>*/ = (100 * CENTIMETRE) * (5 * METRE)

// alternatively a general use reified function:
public inline operator fun <reified T: QuantityType> Quantity<*>.times(that: Quantity<*>): Quantity<T> {
    return Quantity<T>(this.value * that.value)
}
public inline operator fun <reified T: QuantityType> Quantity<*>.div(that: Quantity<*>): Quantity<T> {
    return Quantity<T>(this.value / that.value)
}

// usage: now type declaration is needed for reified function
val area: Quantity<Area> = (100 * CENTIMETRE) * (5 * METRE)
val volume: Quantity<Volume> = area * (2 * METRE)
val length: Quantity<Length> = volume / area
```

Note: quantities cast to a different type will not throw any exceptions at runtime because in the compiled code they are represented entirely as a `double` and have no means to be typed. 

## Example usage
#### Unit creation
Units can be created either as a concrete class or using an enumeration and are both compatible with each other:
```kotlin
val METRE: UnitOfMeasure<Length> = unitOfMeasurement("m", IdentityConverter)
val CENTIMETRE: UnitOfMeasure<Length> = unitOfMeasurement("cm", MetricPrefix.CENTI.converter)
val FEET: UnitOfMeasure<Length> = unitOfMeasurement("yd", 0.3048)

public enum class UnitsOfLength(
    public override val symbol: String?,
    public override val converter: UnitConverter
): UnitOfMeasure<Length> {
    INCH("in", FactorConverter(0.0254)),
    MILE("mi", FactorConverter(1609.344))
}
```
#### Quantities and Conversions
Quantities can be easily created using the methods in QuantityFactory.kt, these utilise Kotlin's operators times and invoke between numbers and a unit.
```kotlin
val fiftyInches = 50 * UnitsOfLength.INCH
val oneMile = 1f(UnitsOfLength.MILE)
val twelveMetres = METRE(12.0)
val twentyFeet = FEET(20L)

// prints 16.66666...
println(fiftyInches convertTo FEET)
// prints 1200 cm
println(twelveMetres.toString(CENTIMETRE))
```
There are lots of other functions quantity has such as relative change and relative difference [Wikipedia: Relative change and difference](https://en.wikipedia.org/wiki/Relative_change_and_difference) and simple mathematics like absolute value, min and max.

## MIT License
```
MIT License

Copyright (c) 2022 Tom Reader

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
