/*
 * Copyright (c) 2021 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("SuperscriptFormat")
package com.gitlab.thomasreader.kuantity.util


/**
 * Helper method for converting an [Int] to its [String] value in superscript form.
 *
 * @return the superscript String representation of this integer
 */
@JvmName("fromInt")
public fun Int.superscriptString(): String = this.toString().let { intString ->
    buildString(intString.length) {
        intString.forEach { character ->
            this@buildString.append(
                when (character) {
                    '-' -> '\u207b'
                    '0' -> '\u2070'
                    '1' -> '\u00B9'
                    '2' -> '\u00B2'
                    '3' -> '\u00B3'
                    '4' -> '\u2074'
                    '5' -> '\u2075'
                    '6' -> '\u2076'
                    '7' -> '\u2077'
                    '8' -> '\u2078'
                    '9' -> '\u2079'
                    else -> throw IllegalStateException("Should not occur")
                }
            )
        }
    }
}