/*
 * Copyright (c) 2021 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.converter

import kotlin.math.log10
import kotlin.math.pow

/**
 * Logarithmic conversion of any base useful for converting logarithmic units such as the
 * [bel or decibel](https://en.wikipedia.org/wiki/Decibel).
 *
 * @property base the base of this logarithmic conversion
 * @constructor Create Log conversion
 * @throws ArithmeticException when base is negative, zero or one
 */
public class LogConverter(
    public val base: Double
): UnitConverter {
    private val baseLog: Double = log10(base)

    init {
        if (this.base <= 0 || this.base == 1.0) {
            throw ArithmeticException("Invalid logarithm of base $base")
        }
    }

    public override fun toReference(value: Double): Double {
        return log10(value) / this.baseLog
    }

    public override fun fromReference(referenceValue: Double): Double {
        return this.base.pow(referenceValue)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LogConverter

        if (base != other.base) return false

        return true
    }

    override fun hashCode(): Int {
        return base.hashCode()
    }
}