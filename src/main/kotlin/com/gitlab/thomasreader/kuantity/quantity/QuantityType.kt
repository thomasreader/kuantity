/*
 * Copyright (c) 2021 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.quantity

public interface QuantityType

// base
public interface Dimensionless : QuantityType
public interface SolidAngle : QuantityType
public interface PlaneAngle : QuantityType
public interface Length : QuantityType
public interface Mass : QuantityType
public interface Time : QuantityType
public interface ElectricCurrent : QuantityType
public interface Temperature : QuantityType
public interface AmountOfSubstance : QuantityType
public interface LuminousIntensity : QuantityType

// derived
public interface Acceleration : QuantityType
public interface Action : QuantityType
public interface Area : QuantityType
public interface Capacitance : QuantityType
public interface Density : QuantityType
public interface DynamicViscosity : QuantityType
public interface ElectricCharge : QuantityType
public interface ElectricDipole : QuantityType
public interface ElectricPotential : QuantityType
public interface ElectricalResistance : QuantityType
public interface Energy : QuantityType
public interface Flow : QuantityType
public interface Force : QuantityType
public interface Frequency : QuantityType
public interface Illuminance : QuantityType
public interface Inductance : QuantityType
public interface Information : QuantityType
public interface InformationEntropy : QuantityType
public interface KinematicViscosity : QuantityType
public interface Luminance : QuantityType
public interface LuminousFlux : QuantityType
public interface MagneticFlux : QuantityType
public interface MagneticFluxDensity : QuantityType
public interface Power : QuantityType
public interface Pressure : QuantityType
public interface RadiationAbsorbedDose : QuantityType
public interface RadiationEquivalentDose : QuantityType
public interface RadiationExposure : QuantityType
public interface RadiationSourceActivity : QuantityType
public interface Speed : QuantityType
public interface Torque : QuantityType
public interface Volume : QuantityType