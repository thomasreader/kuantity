/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("QuantityFormat")
package com.gitlab.thomasreader.kuantity.quantity.format

import com.gitlab.thomasreader.kuantity.quantity.Quantity
import com.gitlab.thomasreader.kuantity.quantity.QuantityType
import com.gitlab.thomasreader.kuantity.unit.UnitOfMeasure
import java.text.NumberFormat

internal val String?.isSpecialUnitSymbol: Boolean get() {
    return this != null && this.length == 1 && when (this[0]) {
        '\'', '"', '°', '′', '″', '‴', '⁗' -> true
        else -> false
    }
}

/**
 * Helper method to format the quantity using the units symbol and the number formatter.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param unit the unit to convert to and format with
 * @param numberFormat the number formatter to format the value with
 * @return symbol string representation
 */
@JvmName("toSymbolString")
public fun <T: QuantityType> Quantity<T>.toSymbolString(
    unit: UnitOfMeasure<T>,
    numberFormat: NumberFormat
): String {
    return unit.formatSymbol(numberFormat.format(this convertTo unit))
}

/**
 * Helper method to format the quantity using the units symbol. This method checks to see whether the unit's symbol is
 * a special case and does not need to be separated by a space e.g. for degrees, arc second and arc minute.
 *
 * @param stringVal the formatted string value of a quantity with this unit
 * @return symbol string representation
 */
public fun UnitOfMeasure<*>.formatSymbol(stringVal: String): String {
    return when (this.symbol.isNullOrBlank()) {
        true -> stringVal
        false -> if (this.symbol.isSpecialUnitSymbol) "$stringVal$symbol" else "$stringVal\u00A0$symbol"
    }
}