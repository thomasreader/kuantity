/*
 * Copyright (c) 2021 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.quantity

/**
 * Mapping function to more easily map the inner [Double] value of the [Quantity].
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param mapper the mapping block
 * @return the mapped quantity
 */
public inline fun <T: QuantityType> Quantity<T>.mapValue(mapper: (Double) -> Double): Quantity<T> = Quantity<T>(mapper(this.value))

/**
 * Mapping function to more easily map the inner [Double] value of the [Quantity] when the predicate returns true.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param predicate the predicate to decide whether the mapper is run or not
 * @param mapper the mapping block
 * @return the mapped quantity if the predicate was true, if false then returns this
 */
public inline fun <T: QuantityType> Quantity<T>.mapValueIf(predicate: (Double) -> Boolean, mapper: (Double) -> Double): Quantity<T> {
    return when (predicate(this.value)) {
        true -> this.mapValue(mapper)
        false -> this
    }
}

/**
 * Mapping function to map the [Quantity].
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param mapper the mapping block
 * @return the mapped quantity
 */
public inline fun <T: QuantityType> Quantity<T>.map(mapper: (Quantity<T>) -> Quantity<T>): Quantity<T> = mapper(this)

/**
 * Mapping function to map the [Quantity] when the predicate returns true.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param predicate the predicate to decide whether the mapper is run or not
 * @param mapper the mapping block
 * @return the mapped quantity if the predicate was true, if false then returns this
 */
public inline fun <T: QuantityType> Quantity<T>.mapIf(
    predicate: (Quantity<T>) -> Boolean,
    mapper: (Quantity<T>) -> Quantity<T>
): Quantity<T> {
    return when (predicate(this)) {
        true -> this.map(mapper)
        false -> this
    }
}