/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.quantity

import com.gitlab.thomasreader.kuantity.quantity.format.formatSymbol
import com.gitlab.thomasreader.kuantity.quantity.range.QuantityRange
import com.gitlab.thomasreader.kuantity.unit.UnitOfMeasure
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import kotlin.math.*

public typealias Qty<T> = Quantity<T>

/**
 * Models a [quantity](https://en.wikipedia.org/wiki/Quantity) using dimensions rather than a value and unit. Using
 * dimensions allows this implementation to use an inline class lowering the footprint when using this.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @property value the value of this quantity in this dimension
 * @constructor Create Quantity
 */
@JvmInline
public value class Quantity<T: QuantityType>(public val value: Double): Comparable<Quantity<T>> {

    init {
        this.value.normaliseZero()
    }

    /**
     * Returns a double which is equal to this quantity in the target unit.
     *
     * @param unit the unit to convert to
     * @return the value in the provided unit
     */
    public infix fun convertTo(unit: UnitOfMeasure<T>): Double {
        return unit.converter.fromReference(this.value).normaliseZero()
    }

    /**
     * Returns a long which is equal to this quantity in the target unit.
     *
     * @param unit the unit to convert to
     * @return the value in the provided unit
     */
    public infix fun convertToLong(unit: UnitOfMeasure<T>): Long {
        return (this convertTo unit).toLong()
    }

    /**
     * Returns an integer which is equal to this quantity in the target unit.
     *
     * @param unit the unit to convert to
     * @return the value in the provided unit
     */
    public infix fun convertToInt(unit: UnitOfMeasure<T>): Int {
        return (this convertTo unit).toInt()
    }

    // MATHEMATICS
    /**
     * Increments this quantity by one.
     *
     * @return this + 1
     */
    public operator fun inc(): Quantity<T> = Quantity<T>(this.value.inc().normaliseZero())

    /**
     * Decrements this quantity by one.
     *
     * @return this - 1
     */
    public operator fun dec(): Quantity<T> = Quantity<T>(this.value.dec().normaliseZero())

    /**
     * Returns this.
     *
     * @return this
     */
    public operator fun unaryPlus(): Quantity<T> = this

    /**
     * Returns a quantity whose value is this * -1.
     *
     * @return this * -1
     */
    public operator fun unaryMinus(): Quantity<T> = Quantity<T>((-this.value).normaliseZero())

    /**
     * The absolute value of this.
     */
    public val absoluteValue: Quantity<T> get() = Quantity<T>(this.value.absoluteValue)

    /**
     * Returns a quantity whose new value is equal to this plus the addend.
     *
     * @param addend the quantity to be added to this
     * @return
     */
    public operator fun plus(addend: Quantity<T>): Quantity<T> = Quantity<T>((this.value + addend.value).normaliseZero())

    /**
     * Returns a quantity whose new value is equal to this minus the subtrahend.
     *
     * @param subtrahend the quantity to be subtracted from this
     * @return
     */
    public operator fun minus(subtrahend: Quantity<T>): Quantity<T> =
        Quantity<T>((this.value - subtrahend.value).normaliseZero())

    /**
     * Multiply this quantity by a primitive scale preserving its dimension.
     *
     * @param multiplier the multiplier
     * @return this * multiplier
     */
    public operator fun times(multiplier: Double): Quantity<T> = Quantity<T>((this.value * multiplier).normaliseZero())

    /**
     * Divide this quantity by a quantity of the same dimension returning a dimensionless quantity.
     *
     * @param divisor the quantity to divide this by
     * @return a dimensionless quantity: this / divisor
     */
    public operator fun div(divisor: Quantity<T>): Quantity<Dimensionless> {
        return Quantity<Dimensionless>((this.value / divisor.value).normaliseZero())
    }

    /**
     * Divide this quantity by a primitive scale preserving its dimension.
     *
     * @param divisor the divisor
     * @return this / divisor
     */
    public operator fun div(divisor: Double): Quantity<T> = Quantity<T>((this.value / divisor).normaliseZero())

    /**
     * Get the remainder when dividing this quantity by a quantity of the same dimension, returning a dimensionless
     * quantity of the remainder.
     *
     * @param divisor the quantity to divide this by
     * @return a dimensionless quantity: this % divisor
     */
    public operator fun rem(divisor: Quantity<T>): Quantity<Dimensionless> =
        Quantity<Dimensionless>((this.value % divisor.value).normaliseZero())

    /**
     * Get the remainder from dividing this quantity by a primitive scale preserving its dimension.
     *
     * @param divisor the divisor
     * @return this % divisor
     */
    public operator fun rem(divisor: Double): Quantity<T> = Quantity<T>((this.value % divisor).normaliseZero())


    /**
     * In any quantitative science, the terms relative change and relative difference are
     * used to compare two quantities while taking into account the "sizes" of the values
     * being compared. The comparison is expressed as a ratio and is a dimensionless and unit-less number.
     *
     * By multiplying these ratios by 100 they can be expressed as percentages so the terms
     * percentage change, percent(age) difference, or relative percentage difference are also
     * commonly used.
     *
     * The relative change between this and the reference value is defined as:
     * {@code (this - reference) / abs(reference)}
     *
     * Note: Combining this with [absoluteValue] will perform an absolute relative change.
     *
     * @param reference the reference value to calculate the relative change
     * @return (this - reference) / abs(reference)
     * @see [Wikipedia: Relative change and difference]](https://en.wikipedia.org/wiki/Relative_change_and_difference)
     */
    public infix fun relativeChange(reference: Quantity<T>): Quantity<Dimensionless> {
//        if (reference.value == 0.0 || reference.value == -0.0) {
//            throw ArithmeticException("Reference value is ${reference.value}, cannot divide by ${reference.value}")
//        }
        return Quantity<Dimensionless>(((this.value - reference.value) / reference.value.absoluteValue).normaliseZero())
    }

    /**
     * In any quantitative science, the terms relative change and relative difference are
     * used to compare two quantities while taking into account the "sizes" of the values
     * being compared. The comparison is expressed as a ratio and is a dimensionless and unit-less number.
     *
     * By multiplying these ratios by 100 they can be expressed as percentages so the terms
     * percentage change, percent(age) difference, or relative percentage difference are also
     * commonly used.
     *
     * If the relationship of the value with respect to the reference value
     * (that is, larger or smaller) does not matter in a particular application,
     * the absolute difference may be used in place of the actual change in the above formula
     * to produce a value for the relative change which is always non-negative.
     *
     * @param that the quantity to compare with
     * @param referenceFun the function which takes this value and that value and computes a value
     * @return (abs(this - that)) / abs(referenceFun(this, that))
     * @see [Wikipedia: Relative change and difference]](https://en.wikipedia.org/wiki/Relative_change_and_difference)
     */
    public fun relativeDifference(
        that: Quantity<T>,
        referenceFun: (thisVal: Double, thatVal: Double) -> Double = { thisVal, thatVal -> max(thisVal, thatVal) }
    ): Quantity<Dimensionless> {
        if (this.value == that.value) {
            return Quantity<Dimensionless>(0.0)
        }
        val functionResult = referenceFun(this.value, that.value)
        return Quantity<Dimensionless>(((this.value - that.value).absoluteValue) / functionResult.absoluteValue)
    }

    // RANGES
    /**
     * Creates a [QuantityRange] with this being the [QuantityRange.start] and that being the [QuantityRange.endInclusive]
     *
     * @param that the quantity to be the inclusive end in a quantity range
     * @return this..that
     */
    public operator fun rangeTo(that: Quantity<T>): QuantityRange<T> {
        return QuantityRange<T>(this, that)
    }

    /**
     * Creates a [QuantityRange] by adding to and subtracting from this quantity.
     *
     * @param delta the plus or minus value to create the range from
     * @return (this-abs(delta))..(this+abs(delta))
     */
    public infix fun plusOrMinus(delta: Double): QuantityRange<T> {
        return when (delta >= 0.0) {
            true -> Quantity<T>(this.value - delta)..Quantity<T>(this.value + delta)
            false -> Quantity<T>(this.value + delta)..Quantity<T>(this.value - delta)
        }
    }

    /**
     * Creates a [QuantityRange] by adding to and subtracting from this quantity.
     *
     * @param delta the plus or minus quantity to create the range from
     * @return (this-abs(delta))..(this+abs(delta))
     */
    public infix fun plusOrMinus(delta: Quantity<T>): QuantityRange<T> {
        return this.plusOrMinus(delta.value)
    }

    // COMPARISONS
    /**
     * Compares two quantities to provide a natural ordering with their value.
     *
     * @param that the quantity to compare with
     * @return the value zero if the quantities are equal; a value less then zero if this is less than than that;
     * a value greater than zero if this is greater than that
     */
    public override operator fun compareTo(other: Quantity<T>): Int = this.value.compareTo(other.value)

    /**
     * Compares two quantities with an almost-equal function to deduce a natural ordering with their value.
     *
     * The function provides a way of seeing whether the floating-point values are close-enough that is considered OK
     * to treat them as equal; this could be using a machine epsilon, ulp distance from each other, the relative
     * difference or a different algorithm. If the function considers them not almost-equal then the standard
     * [compareTo] will be called.
     *
     * @param that the quantity to compare with
     * @param almostEqual the algorithm to decide whether both quantities are almost-equal
     * @return the value zero if the quantities are equal or almost-equal; a value less then zero if this is less
     * than than that; a value greater than zero if this is greater than that
     */
    public inline fun compareTo(that: Quantity<T>, almostEqual: (thisVal: Double, thatVal: Double) -> Boolean): Int {
        return when (almostEqual(this.value, that.value)) {
            true -> 0
            false -> this.compareTo(that)
        }
    }

    /**
     * Returns the smallest quantity of this or that.
     *
     * @param that the quantity to compare with
     * @return the smallest quantity
     */
    public infix fun min(that: Quantity<T>): Quantity<T> {
        return when (this.value <= that.value) {
            true -> this
            false -> that
        }
    }

    /**
     * Returns the largest quantity of this or that.
     *
     * @param that the quantity to compare with
     * @return the largest quantity
     */
    public infix fun max(that: Quantity<T>): Quantity<T> {
        return when (this.value >= that.value) {
            true -> this
            false -> that
        }
    }

    /**
     * Returns whether this quantity is finite.
     *
     * @return true if this quantity is finite, false otherwise
     */
    public fun isFinite(): Boolean = this.value.isFinite()

    /**
     * Returns whether this quantity is infinite.
     *
     * @return true if this quantity is infinite, false otherwise
     */
    public fun isInfinite(): Boolean = this.value.isInfinite()

    /**
     * Returns the sign of this quantity.
     *
     * -1.0 if negative, 0.0 if zero, 1.0 if positive or NaN if NaN.
     */
    public val sign: Double get() = this.value.sign

    /**
     * Returns whether this quantity is greater than zero.
     *
     * @return true if this > 0, false otherwise
     */
    public fun isPositive(): Boolean = this.sign == 1.0

    /**
     * Returns whether this quantity is negative.
     *
     * @return true if this < 0, false otherwise
     */
    public fun isNegative(): Boolean = this.sign == -1.0

    /**
     * Returns whether this quantity is zero.
     *
     * @return true if this == 0, false otherwise
     */
    public fun isZero(): Boolean = this.sign == 0.0

    /**
     * Returns whether this quantity is zero or nearly zero using an epsilon for a tolerance.
     *
     * An epsilon is useful when dealing with floating point numbers near zero as other methods such as relative
     * difference or ulp distance are sub-par for comparing almost-equal floating-points when approaching zero.
     *
     * @param epsilon the machine tolerance
     * @return true if abs(this) - abs(epsilon) <= 0, false otherwise
     */
    public fun isZero(epsilon: Double): Boolean {
        return when (this.value >= 0) {
            true -> this.value <= epsilon
            false -> this.value.absoluteValue <= epsilon
        }
    }
//    public val uncertainty: Double get() {
//        return when (this.value) {
//            Double.NaN -> Double.NaN
//            Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY -> Double.POSITIVE_INFINITY
//            Double.MAX_VALUE, -Double.MAX_VALUE -> (Double.MAX_VALUE - Double.MAX_VALUE.nextDown()) / 2.0
//            0.0, -0.0 -> (0.0.nextUp() - 0.0) / 2.0
//            else -> {
//                val absThis = this.value.absoluteValue
//                return (max(absThis - absThis.nextDown(), absThis.nextUp() - absThis)).let {
//                    when (it == Double.MIN_VALUE) {
//                        true -> Double.MIN_VALUE
//                        false -> it / 2.0
//                    }
//                }
//            }
//        }
//    }

    /**
     * Returns a string representation of this quantity in the provided unit using its symbol.
     *
     * @param unit the unit to convert this quantity to
     * @return the string representation of this quantity in that unit
     */
    public fun toString(unit: UnitOfMeasure<T>): String {
        val result = this convertTo unit
        val formattedResult = when (result < 1e11) {
            true -> smallDoubleFormat
            false -> bigDoubleFormat
        }.format(result)
        return unit.formatSymbol(formattedResult)
    }
}

private inline fun Double.normaliseZero(): Double = this + 0.0
internal val smallDoubleFormat: DecimalFormat = DecimalFormat("0.###", DecimalFormatSymbols.getInstance())
internal val bigDoubleFormat: DecimalFormat = DecimalFormat("0E0").apply { minimumFractionDigits = 2 }

/**
 * Helper function to multiply a quantity by a dimensionless quantity (retaining its dimension).
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param multiplier the dimensionless quantity
 * @return this * multiplier
 */
public inline operator fun <T: QuantityType> Quantity<T>.times(multiplier: Quantity<Dimensionless>): Quantity<T> {
    return this * multiplier.value
}

/**
 * Helper function to divide a quantity by a dimensionless quantity (retaining its dimension).
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param divisor the dimensionless quantity
 * @return this / divisor
 */
public inline operator fun <T: QuantityType> Quantity<T>.div(divisor: Quantity<Dimensionless>): Quantity<T> {
    return this / divisor.value
}

/**
 * Helper function to get the remainder of a quantity divided by a dimensionless quantity (retaining its dimension).
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param divisor the dimensionless quantity
 * @return this % divisor
 */
public inline operator fun <T: QuantityType> Quantity<T>.rem(divisor: Quantity<Dimensionless>): Quantity<T> {
    return this % divisor.value
}