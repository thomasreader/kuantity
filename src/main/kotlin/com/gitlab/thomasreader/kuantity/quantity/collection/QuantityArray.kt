/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.quantity.collection

import com.gitlab.thomasreader.kuantity.quantity.Quantity
import com.gitlab.thomasreader.kuantity.quantity.QuantityType

/**
 * Helper method to create a quantity array of a set size.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param size the size of the quantity array
 * @return a new quantity array with quantity values of zero
 */
public fun <T: QuantityType> quantityArrayOf(size: Int): QuantityArray<T> {
    return QuantityArray<T>(DoubleArray(size))
}

/**
 * Helper method to create a quantity array of a set size and run a block of code on the newly created array.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param size the size of the quantity array
 * @param block the block to run on the array
 * @return a new quantity array run with the block
 */
public inline fun <T: QuantityType> quantityArrayOf(size: Int, block: QuantityArray<T>.() -> Unit): QuantityArray<T> {
    return QuantityArray<T>(DoubleArray(size)).apply { block(this) }
}

/**
 * Inline array for quantities of the same dimension.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @property array the inner array
 * @constructor Create Quantity array
 */
@JvmInline
public value class QuantityArray<T: QuantityType>(private val array: DoubleArray): Collection<Quantity<T>> {
    /**
     * Get the size of this array.
     */
    public override val size: Int
        get() = this.array.size

    /**
     * To double array
     *
     * @return double array with the quantity values
     */
    public fun toDoubleArray(): DoubleArray = this.array

    /**
     * Get the quantity at the specified index. This method can be called using the index operator.
     *
     * If the index is out of bounds of this array, throws an [IndexOutOfBoundsException].
     *
     * @param index index to retrieve
     * @return the quantity at this index
     */
    public operator fun get(index: Int): Quantity<T> = Quantity<T>(this.array[index])

    /**
     * Sets the quantity at the specified index. This method can be called using the index operator.
     *
     * @param index index to insert the quantity at
     * @param quantity the quantity to add at the index
     */
    public operator fun set(index: Int, quantity: Quantity<T>) { this.array[index] = quantity.value }

    public override fun contains(element: Quantity<T>): Boolean = any { it == element }

    public override fun containsAll(elements: Collection<Quantity<T>>): Boolean {
        elements.forEach { element ->
            if (!any { it == element }) {
                return false
            }
        }
        return true
    }

    public override fun isEmpty(): Boolean = array.isEmpty()

    public override fun iterator(): QuantityIterator<T> {
        return QuantityIterator(this)
    }

    /**
     * Inline helper method to more efficiently iterate over this array. Using this function rather than [forEach] will
     * mean that the quantity isn't boxed and then unboxed in use.
     *
     * @param operation the operation to run on each quantity
     */
    public inline fun forEachQuantity(operation: (Quantity<T>) -> Unit) {
        val iterator = this.iterator()
        while (iterator.hasNext()) {
            operation(iterator.next())
        }
    }

    /**
     * Quantity iterator.
     *
     * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
     * @property quantityArray the array to be iterated
     * @constructor Create Quantity iterator
     */
    public class QuantityIterator<T: QuantityType>(
        private val quantityArray: QuantityArray<T>
    ): Iterator<Quantity<T>> {
        private var index: Int = 0

        public override fun hasNext(): Boolean = this.index < quantityArray.size

        public override fun next(): Quantity<T> {
            val result = this.quantityArray[this.index]
            this.index++
            return result
        }
    }
}

