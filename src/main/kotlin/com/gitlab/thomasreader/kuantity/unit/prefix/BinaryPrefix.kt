/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.unit.prefix

import com.gitlab.thomasreader.kuantity.converter.MultiplicativeConverter
import com.gitlab.thomasreader.kuantity.converter.PowerConverter

/**
 * Models a [binary prefix](https://en.wikipedia.org/wiki/Binary_prefix) usually used for units such as the bit or byte.
 *
 * @property symbol the symbol of the prefix
 * @param exponent the exponent raised to a base of two this prefix symbolises
 */
public enum class BinaryPrefix(
    public override val symbol: String,
    exponent: Int
) : UnitPrefix {
    /**
     * Yobi 2<sup>80</sup>
     */
    YOBI("Yi", 80),

    /**
     * Zebi 2<sup>70</sup>
     */
    ZEBI("Zi", 70),

    /**
     * Exbi 2<sup>60</sup>
     */
    EXBI("Ei", 60),

    /**
     * Pebi 2<sup>50</sup>
     */
    PEBI("Pi", 50),

    /**
     * Tebi 2<sup>40</sup>
     */
    TEBI("Ti", 40),

    /**
     * Gibi 2<sup>30</sup>
     */
    GIBI("Gi", 30),

    /**
     * Mebi 2<sup>20</sup>
     */
    MEBI("Mi", 20),

    /**
     * Kibi 2<sup>10</sup>
     */
    KIBI("Ki", 10);

    public override val converter: MultiplicativeConverter

    init {
        converter = PowerConverter(2, exponent)
    }
}