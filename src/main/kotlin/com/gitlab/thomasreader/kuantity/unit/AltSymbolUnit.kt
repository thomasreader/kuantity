/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.unit

import com.gitlab.thomasreader.kuantity.converter.UnitConverter
import com.gitlab.thomasreader.kuantity.quantity.QuantityType

/**
 * Models a unit of measurement which has an alternate symbol that can be used, e.g. miles per hour: mi/h or mph.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @property compositeUnit the unit of measurement which is identical to this bar symbol
 * @constructor
 * @param symbol the alternate symbol or abbreviation for the composite unit
 */
public open class AltSymbolUnit<T: QuantityType>(
    public val compositeUnit: UnitOfMeasure<T>,
    public final override val symbol: String
): UnitOfMeasure<T> {
    override val converter: UnitConverter
        get() = this.compositeUnit.converter

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AltSymbolUnit<*>

        if (compositeUnit != other.compositeUnit) return false
        if (symbol != other.symbol) return false

        return true
    }

    override fun hashCode(): Int {
        return 31 * compositeUnit.hashCode() + symbol.hashCode()
    }
}

/**
 * Creates a unit of measurement with an alternate symbol composed of this and the symbol
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param symbol the alternate symbol or abbreviation for this unit
 * @return duplicate unit of measurement with an alternate symbol
 */
public fun <T: QuantityType> UnitOfMeasure<T>.alternateSymbol(symbol: String): UnitOfMeasure<T> {
    return AltSymbolUnit(this, symbol)
}