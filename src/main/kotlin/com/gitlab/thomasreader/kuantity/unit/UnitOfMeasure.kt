/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.unit

import com.gitlab.thomasreader.kuantity.converter.*
import com.gitlab.thomasreader.kuantity.quantity.QuantityType

public typealias UOM<T> = UnitOfMeasure<T>

/**
 * Models a simple [unit of measurement](https://en.wikipedia.org/wiki/Unit_of_measurement) with a nullable symbol and
 * [UnitConverter] to convert between units of the same [dimension](https://en.wikipedia.org/wiki/Dimensional_analysis).
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @property symbol the symbol or abbreviation of this unit — nullable
 * @property converter the conversion to convert to and from the reference unit of measurement
 */
public interface UnitOfMeasure<T: QuantityType> {
    public val symbol: String?
    public val converter: UnitConverter
}


/**
 * Models a unit with a symbol and a {@link UnitConversion} used to convert to and from a reference unit.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @property converter the conversion to convert to and from the reference unit of measurement
 * @constructor
 * @param symbol the symbol or abbreviation of this unit — nullable
 */
public open class UnitOfMeasureImpl<T: QuantityType> (
    symbol: String?,
    public override val converter: UnitConverter
): UnitOfMeasure<T> {
    public final override val symbol: String? = if (symbol.isNullOrBlank()) null else symbol
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UnitOfMeasureImpl<*>

        if (converter != other.converter) return false
        if (symbol != other.symbol) return false

        return true
    }

    override fun hashCode(): Int {
        return 31 * converter.hashCode() + (symbol?.hashCode() ?: 0)
    }
}