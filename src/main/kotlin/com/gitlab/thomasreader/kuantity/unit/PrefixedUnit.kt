/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.unit

import com.gitlab.thomasreader.kuantity.converter.UnitConverter
import com.gitlab.thomasreader.kuantity.converter.append
import com.gitlab.thomasreader.kuantity.quantity.QuantityType
import com.gitlab.thomasreader.kuantity.unit.prefix.UnitPrefix
import com.gitlab.thomasreader.kuantity.unit.prefix.prefixSymbol
import com.gitlab.thomasreader.kuantity.unit.prefix.MetricPrefix

/**
 * Models a unit of measurement composed of both a [UnitPrefix] and a [UnitOfMeasure], for example a kilogram is
 * composed of [MetricPrefix.KILO] and the unit gram.
 *
 * Precaution must be used when dealing with derived units without a special unit symbol: a cubic decimetre is not equal
 * to prefixing a cubic metre with [MetricPrefix.DECI] as the prefix should have been raised to the power of three.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @property prefix the prefix which is prefixing unit
 * @property unit the unit that is being prefixed
 * @constructor Create Prefixed unit
 * @throws IllegalArgumentException if unit is a [PrefixedUnit] or if the [UnitOfMeasure.symbol] is null
 */
public open class PrefixedUnit<T: QuantityType>(
    public val prefix: UnitPrefix,
    public val unit: UnitOfMeasure<T>
): UnitOfMeasure<T> {
    init {
        if (this.unit is PrefixedUnit)
            throw IllegalArgumentException("Provided unit is already prefixed, compound prefixes are not permitted")

        if (this.unit.symbol.isNullOrBlank())
            throw IllegalArgumentException("Provided unit is symbol-less and cannot be prefixed")
    }

    override val converter: UnitConverter = this.prefix.converter.append(this.unit.converter)
    override val symbol: String? get() = this.prefix.prefixSymbol(this.unit)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PrefixedUnit<*>

        if (prefix != other.prefix) return false
        if (unit != other.unit) return false

        return true
    }

    override fun hashCode(): Int {
        return 31 * prefix.hashCode() + unit.hashCode()
    }
}

/**
 * Helper method to easily prefix a unit of measurement.
 *
 * Precaution must be used when dealing with derived units without a special unit symbol: a cubic decimetre is not equal
 * to prefixing a cubic metre with [MetricPrefix.DECI] as the prefix should have been raised to the power of three.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param prefix the prefix to use
 * @return a new prefixed unit
 */
public fun <T: QuantityType> UnitOfMeasure<T>.prefixedBy(prefix: UnitPrefix): PrefixedUnit<T> {
    return PrefixedUnit<T>(prefix, this)
}

/**
 * Helper method to easily prefix a unit of measurement.
 *
 * Precaution must be used when dealing with derived units without a special unit symbol: a cubic decimetre is not equal
 * to prefixing a cubic metre with [MetricPrefix.DECI] as the prefix should have been raised to the power of three.
 *
 * @param T the type of quantity this unit belongs to e.g. metre is a unit of *length*
 * @param unit the unit to be prefixed
 * @return a new prefixed unit
 */
public operator fun <T: QuantityType> UnitPrefix.invoke(unit: UnitOfMeasure<T>): PrefixedUnit<T> = unit.prefixedBy(this)