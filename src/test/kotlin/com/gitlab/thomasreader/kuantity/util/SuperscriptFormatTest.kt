package com.gitlab.thomasreader.kuantity.util

import org.junit.Test

import org.junit.Assert.*

class SuperscriptFormatTest {

    @Test
    fun fromInt() {
        assertEquals(
            "\u207b\u00B2\u00B9\u2074\u2077\u2074\u2078\u00B3\u2076\u2074\u2078",
            Int.MIN_VALUE.superscriptString()
        )

        assertEquals(
            "\u00B2\u00B9\u2074\u2077\u2074\u2078\u00B3\u2076\u2074\u2077",
            Int.MAX_VALUE.superscriptString()
        )

        assertEquals("\u2070", 0.superscriptString())

        assertEquals("\u00B3", 3.superscriptString())
    }
}