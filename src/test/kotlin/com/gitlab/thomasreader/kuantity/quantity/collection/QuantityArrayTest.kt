/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.quantity.collection

import com.gitlab.thomasreader.kuantity.quantity.Quantity
import org.junit.Test

import org.junit.Assert.*

class QuantityArrayTest {

    @Test
    fun contains() {
        val oneTwoThree = quantityArrayOf<Nothing>(3) {
            set(0, Quantity(1.0))
            set(1, Quantity(2.0))
            set(2, Quantity(3.0))
        }
        assertTrue(oneTwoThree.contains(Quantity(1.0)))
        assertFalse(oneTwoThree.contains(Quantity(4.0)))
    }

    @Test
    fun containsAll() {
        val oneTwoThree = quantityArrayOf<Nothing>(3) {
            set(0, Quantity(1.0))
            set(1, Quantity(2.0))
            set(2, Quantity(3.0))
        }
        val oneTwo = quantityArrayOf<Nothing>(2) {
            set(0, Quantity(1.0))
            set(1, Quantity(2.0))
        }

        assertTrue(oneTwoThree.containsAll(oneTwo))
        assertFalse(oneTwo.containsAll(oneTwoThree))
    }

    @Test
    fun forEachQuantity() {
        val oneTwoThree = quantityArrayOf<Nothing>(3) {
            set(0, Quantity(1.0))
            set(1, Quantity(2.0))
            set(2, Quantity(3.0))
        }
        var accumulator: Quantity<Nothing> = Quantity(0.0)
        oneTwoThree.forEachQuantity {
            accumulator = accumulator + it
        }
        assertEquals(6.0, accumulator.value, 1.0E-7)
    }
}