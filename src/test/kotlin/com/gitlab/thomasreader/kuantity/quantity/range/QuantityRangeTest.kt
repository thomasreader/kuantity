/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.quantity.range

import com.gitlab.thomasreader.kuantity.converter.UnitConverter
import com.gitlab.thomasreader.kuantity.converter.FactorConverter
import com.gitlab.thomasreader.kuantity.quantity.Length
import com.gitlab.thomasreader.kuantity.quantity.invoke
import com.gitlab.thomasreader.kuantity.unit.UnitOfMeasure
import com.gitlab.thomasreader.kuantity.unit.prefix.MetricPrefix
import org.junit.Test

import org.junit.Assert.*

class QuantityRangeTest {

    val centimetre = object : UnitOfMeasure<Length> {
        override val symbol: String?
            get() = "cm"
        override val converter: UnitConverter
            get() = MetricPrefix.CENTI.converter
    }

    val inch = object : UnitOfMeasure<Length> {
        override val symbol: String?
            get() = "in"
        override val converter: UnitConverter
            get() = FactorConverter(0.0254)
    }

    @Test
    fun contains() {
        val zeroToTenCM = QuantityRange(0.0(centimetre), 10.0(centimetre))
        val infiniteRange = QuantityRange(Double.NEGATIVE_INFINITY(centimetre), Double.POSITIVE_INFINITY(centimetre))
        assertTrue(zeroToTenCM.contains(5.0(centimetre)))
        assertTrue(zeroToTenCM.contains(2.0(inch)))
        assertFalse(zeroToTenCM.contains(4.0(inch)))
        assertTrue(infiniteRange.contains(Double.POSITIVE_INFINITY(inch)))
    }
}