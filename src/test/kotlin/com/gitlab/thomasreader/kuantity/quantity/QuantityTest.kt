/*
 * Copyright (c) 2022 Tom Reader
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.gitlab.thomasreader.kuantity.quantity

import com.gitlab.thomasreader.kuantity.converter.UnitConverter
import com.gitlab.thomasreader.kuantity.converter.IdentityConverter
import com.gitlab.thomasreader.kuantity.unit.UnitOfMeasure
import com.gitlab.thomasreader.kuantity.unit.prefix.MetricPrefix
import org.junit.Test

import org.junit.Assert.*
import kotlin.math.absoluteValue
import kotlin.math.min

class QuantityTest {

    val metre = object : UnitOfMeasure<Length> {
        override val symbol: String?
            get() = "m"
        override val converter: UnitConverter
            get() = IdentityConverter
    }

    val centimetre = object : UnitOfMeasure<Length> {
        override val symbol: String?
            get() = "cm"
        override val converter: UnitConverter
            get() = MetricPrefix.CENTI.converter
    }

    @Test
    fun init() {
        assertEquals(0.0, Quantity<Nothing>(-0.0).value, 0.0)
    }

    @Test
    fun convertTo() {
        assertEquals(100.0, Quantity<Length>(1.0).convertTo(centimetre), 1.0E-7)
        assertEquals(1.0, Quantity<Length>(1.0).convertTo(metre), 1.0E-7)
    }

    @Test
    fun convertToLong() {
        assertEquals(100L, Quantity<Length>(1.0).convertToLong(centimetre))
        assertEquals(1L, Quantity<Length>(1.0).convertToLong(metre))
    }

    @Test
    fun convertToInt() {
        assertEquals(100, Quantity<Length>(1.0).convertToInt(centimetre))
        assertEquals(1, Quantity<Length>(1.0).convertToInt(metre))
    }

    @Test
    fun inc() {
        assertEquals(0.0, Quantity<Nothing>(-1.0).inc().value, 0.0)
    }

    @Test
    fun dec() {
        assertEquals(0.0, Quantity<Nothing>(1.0).dec().value, 0.0)
    }

    @Test
    fun unaryMinus() {
        assertEquals(-1.0, Quantity<Nothing>(1.0).unaryMinus().value, 0.0)
    }

    @Test
    fun getAbsoluteValue() {
        assertEquals(1.0, Quantity<Nothing>(-1.0).absoluteValue.value, 0.0)
    }

    @Test
    fun plus() {
        assertEquals(10.0, (Quantity<Nothing>(5.0) + Quantity<Nothing>(5.0)).value, 0.0)
        assertEquals(0.0, (Quantity<Nothing>(5.0) + Quantity<Nothing>(-5.0)).value, 0.0)
    }

    @Test
    fun minus() {
        assertEquals(0.0, (Quantity<Nothing>(5.0) - Quantity<Nothing>(5.0)).value, 0.0)
        assertEquals(10.0, (Quantity<Nothing>(5.0) - Quantity<Nothing>(-5.0)).value, 0.0)
    }

    @Test
    fun times() {
        assertEquals(10.0, Quantity<Nothing>(1.0).times(10.0).value, 1.0E-7)
    }

    @Test
    fun div() {
        assertEquals(1.0, Quantity<Nothing>(10.0).div(10.0).value, 1.0E-7)
    }

    @Test
    fun divQuantityT() {
        assertEquals(1.0, Quantity<Nothing>(10.0).div(Quantity(10.0)).value, 1.0E-7)
    }

    @Test
    fun rem() {
        assertEquals(1.0, Quantity<Nothing>(5.0).rem(2.0).value, 1.0E-7)
    }

    @Test
    fun remQuantityT() {
        assertEquals(1.0, Quantity<Nothing>(5.0).rem(Quantity(2.0)).value, 1.0E-7)
    }

    @Test
    fun relativeChange() {
        assertEquals(0.0, Quantity<Nothing>(1.0).relativeChange(Quantity(1.0)).value, 0.0)
        assertEquals(0.1, Quantity<Nothing>(1.1).relativeChange(Quantity(1.0)).value, 1.0E-7)
        assertEquals(2.1, Quantity<Nothing>(1.1).relativeChange(Quantity(-1.0)).value, 1.0E-7)
        assertEquals(-2.1, Quantity<Nothing>(-1.1).relativeChange(Quantity(1.0)).value, 1.0E-7)
        assertEquals(
            Double.POSITIVE_INFINITY,
            Quantity<Nothing>(1.1).relativeChange(Quantity(0.0)).value,
            1.0E-7
        )
    }

    @Test
    fun relativeDifference() {
        assertEquals(0.0, Quantity<Nothing>(0.0).relativeDifference(Quantity(0.0)).value, 0.0)
        assertEquals(0.5, Quantity<Nothing>(2.0).relativeDifference(Quantity(1.0)).value, 1.0E-7)
        assertEquals(0.5, Quantity<Nothing>(1.0).relativeDifference(Quantity(2.0)).value, 1.0E-7)

        assertEquals(
            1.0,
            Quantity<Nothing>(2.0).relativeDifference(
                Quantity(1.0)
            ) { thisVal, thatVal -> min(thisVal, thatVal) }.value,
            1.0E-7)

        assertEquals(
            1.0,
            Quantity<Nothing>(1.0).relativeDifference(
                Quantity(2.0)
            ) { thisVal, thatVal -> min(thisVal, thatVal) }.value,
            1.0E-7)
    }

    @Test
    fun plusOrMinus() {
        val fivePlusMinusTwo = Quantity<Nothing>(5.0).plusOrMinus(2.0)
        val fivePlusMinusNegTwo = Quantity<Nothing>(5.0).plusOrMinus(-2.0)

        assertEquals(3.0, fivePlusMinusTwo.start.value, 1.0E-7)
        assertEquals(7.0, fivePlusMinusTwo.endInclusive.value, 1.0E-7)
        assertEquals(3.0, fivePlusMinusNegTwo.start.value, 1.0E-7)
        assertEquals(7.0, fivePlusMinusNegTwo.endInclusive.value, 1.0E-7)
    }

    @Test
    fun compareToAlmostEqual() {
        assertEquals(
            0,
            Quantity<Nothing>(0.0).compareTo(Quantity(5000.0)) { thisVal, thatVal ->
                thisVal.isFinite() && thatVal.isFinite()
            }
        )

        assertEquals(
            0,
            Quantity<Nothing>(0.000001).compareTo(Quantity(0.000002)) { thisVal, thatVal ->
                thisVal.absoluteValue < 0.00001 && thatVal.absoluteValue < 0.00001
            }
        )

        assertEquals(
            -1,
            Quantity<Nothing>(0.000001).compareTo(Quantity(0.000002)) { thisVal, thatVal ->
                thisVal.absoluteValue < 0.0000001 && thatVal.absoluteValue < 0.0000001
            }
        )
    }

    @Test
    fun min() {
        assertEquals(0.0, Quantity<Nothing>(0.0).min(Quantity<Nothing>(1.0)).value, 0.0)
        assertEquals(0.0, Quantity<Nothing>(1.0).min(Quantity<Nothing>(0.0)).value, 0.0)
    }

    @Test
    fun max() {
        assertEquals(1.0, Quantity<Nothing>(0.0).max(Quantity<Nothing>(1.0)).value, 0.0)
        assertEquals(1.0, Quantity<Nothing>(1.0).max(Quantity<Nothing>(0.0)).value, 0.0)
    }

    @Test
    fun isFinite() {
        assertTrue(Quantity<Nothing>(0.0).isFinite())
        assertFalse(Quantity<Nothing>(Double.POSITIVE_INFINITY).isFinite())
    }

    @Test
    fun isInfinite() {
        assertFalse(Quantity<Nothing>(0.0).isInfinite())
        assertTrue(Quantity<Nothing>(Double.POSITIVE_INFINITY).isInfinite())
    }

    @Test
    fun isZeroEpsilon() {
        assertTrue(Quantity<Nothing>(0.0000000000123).isZero(0.0000001))
        assertFalse(Quantity<Nothing>(0.000123).isZero(0.0000001))
    }
}