package com.gitlab.thomasreader.kuantity.converter

import org.junit.Test

import org.junit.Assert.*

class FactorConverterTest {

    val inchConversion = FactorConverter(0.0254)
    val mileConversion = FactorConverter(1609.344)

    @Test
    fun convertToReference() {
        assertEquals(112654.08, mileConversion.toReference(70.0), 1.0E-7)
        assertEquals(2.54, inchConversion.toReference(100.0), 1.0E-7)
    }

    @Test
    fun convertFromReference() {
        assertEquals(62.13711922373339696174, mileConversion.fromReference(100000.0), 1.0E-7)
        assertEquals(39.37007874015748031496, inchConversion.fromReference(1.0), 1.0E-7)
    }

    @Test
    fun pow() {
        assertEquals(1.0, inchConversion.pow(0).factor, 0.0)
        assertEquals(0.00064516, inchConversion.pow(2).factor, 1.0E-7)
    }
}