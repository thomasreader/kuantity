package com.gitlab.thomasreader.kuantity.converter

import org.junit.Test

import org.junit.Assert.*

class OffsetConverterTest {

    val celsiusConversion = OffsetConverter(273.15)

    @Test
    fun convertToReference() {
        assertEquals(303.15, celsiusConversion.toReference(30.0), 1.0E-7)
    }

    @Test
    fun convertFromReference() {
        assertEquals(-273.15, celsiusConversion.fromReference(0.0), 1.0E-7)
    }
}