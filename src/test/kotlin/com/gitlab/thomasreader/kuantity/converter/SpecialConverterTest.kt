package com.gitlab.thomasreader.kuantity.converter

import org.junit.Test

import org.junit.Assert.*

class SpecialConverterTest {

    val fahrenheitConversion = SpecialConverter(
        functionToReference = { (it + 459.67) * 5.0 / 9.0 },
        functionFromReference = { (it * 9.0 / 5.0) - 459.67 }
    )

    @Test
    fun convertToReference() {
        assertEquals(255.92777777777777777778, fahrenheitConversion.toReference(1.0), 1.0E-7)
    }

    @Test
    fun convertFromReference() {
        assertEquals(-459.67, fahrenheitConversion.fromReference(0.0), 1.0E-7)
    }
}