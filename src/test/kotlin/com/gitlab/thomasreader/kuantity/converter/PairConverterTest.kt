package com.gitlab.thomasreader.kuantity.converter

import org.junit.Test

import org.junit.Assert.*

class PairConverterTest {

    val fahrenheitConversion = PairConverter(OffsetConverter(459.67), FactorConverter(5.0/9.0))
    val newtonConversion = PairConverter(FactorConverter(100.0/33.0), OffsetConverter(273.15))

    @Test
    fun convertToReference() {
        assertEquals(255.92777777777777777778, fahrenheitConversion.toReference(1.0), 1.0E-7)
        assertEquals(276.18030303030303030303, newtonConversion.toReference(1.0), 1.0E-7)
    }

    @Test
    fun convertFromReference() {
        assertEquals(-459.67, fahrenheitConversion.fromReference(0.0), 1.0E-7)
        assertEquals(-90.1395, newtonConversion.fromReference(0.0), 1.0E-7)
    }
}